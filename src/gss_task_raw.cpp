/*
    ご注意：
    ArduinoHttpClientライブラリを使うと色々便利ですが、なぜか通信が不安定です。細かくヘッダを送信しているためだと思います。
    送信データはまとめて送ったほうが安定して通信できます。
    GSSはリダイレクトするので、その処理をしないと戻り値が見れません。
    リダイレクトが来なくてもデータはGSSへ届いているケースが多いです。
*/

//Google spread sheet
//https://docs.google.com/spreadsheets/d/19mizSyhRrpybc6K-XhtoE_XoyP883EMy0D7Wj5mbNPs/edit?usp=sharing
//Google Action Script
//https://script.google.com/macros/s/AKfycbwmCahpagnqOWg78rkFSxGvoriXyzvayHOeD-F4rpMm6zaZLshWNWKs44WVI3YhQOGM/exec

#include "gss_task.h"
#include "HL7800ModemClient.h"
#include "modem_setting.h"

long send_period_ms = (60 * 1000);

extern HL7800Modem modemL;
extern HL7800ModemClient client;
bool modem_init();
bool setSSL();

const String _HOST = "script.google.com";
const int _PORT = 443;
const String DEPLOY_ID = "AKfycbwmCahpagnqOWg78rkFSxGvoriXyzvayHOeD-F4rpMm6zaZLshWNWKs44WVI3YhQOGM";
const String _MACRO_URL = "/macros/s/" + DEPLOY_ID + "/exec";

static void doBeep(int mode)
{
    Dm.BeepVolumeCtrl(3);

    if (mode == 0)
    {
        Dm.setPwm(0x03FC);
        vTaskDelay(100);
    }
    else
    {
        Dm.setPwm(0x03FC);
        vTaskDelay(100);
        Dm.setPwm(0);
        vTaskDelay(100);
        Dm.setPwm(0x03FC);
        vTaskDelay(100);
    }
    Dm.setPwm(0);
    Dm.BeepVolumeCtrl(0);
}

String header =
    "POST " + _MACRO_URL + " HTTP/1.1\r\n" +
    "HOST: " + _HOST + ":" + String(_PORT) + "\r\n" +
    "content-type: application/x-www-form-urlencoded\r\n";

bool sendReport(int sendcount,int retrycount)
{
    bool retVal = false;

    SerialDebug.println("#######  Start  sendReport  ########");

    //activation need after wakeup.
    SerialDebug.print("###  Start activate...");
    if (!modemL.activate(APN, USERNAME, PASSWORD))
    {
        SerialDebug.println("Error");
        return false;
    }
    SerialDebug.println("OK");

    int rssi = modemL.getRssi();
    float temp = Dm.readTemperature();
    float press = Dm.readPressure() / 100;
    float hum = Dm.readHumidity();

    String payload = "1=" + String(temp, 1);
    payload += "&2=" + String(press, 1);
    payload += "&3=" + String(hum, 1);
    payload += "&4=" + String(rssi);
    payload += "&5=" + String(Dm.readExADC(), 1);
    payload += "&6=" + String(sendcount);
    payload += "&7=" + String(retrycount);

    String senddata = header;
    senddata += "Content-Length: " + String(payload.length()) + "\r\n";
    senddata += "\r\n";
    senddata += payload;

    SerialDebug.println("### new senddata: ");
    SerialDebug.println(senddata);

    if (client.connect(_HOST.c_str(), _PORT) > 0)
    {
        size_t ret = client.print(senddata);
        if (ret == 0)
        {
            client.stop();
            SerialDebug.println("########## sendReport Send Error !! ###########\r\n");
            doBeep(0);
            return retVal;
        }
        SerialDebug.println("#######  send ok");
        retVal = true;

        SerialDebug.println("#######  send ok,wait response");
        unsigned long timeout = millis();
        while (client.available() == 0)
        {
            SerialDebug.print("@");
            if (millis() - timeout > 10000)
            {
                SerialDebug.println(">>> Client Timeout !");
                client.stop();
                return retVal;
            }
            delay(100);
        }

        String response = client.readString();

        //SerialDebug.println("### response Start");
        //SerialDebug.println(response);
        //SerialDebug.println("### response End...");
		
        //リダイレクトを処理します。リダイレクトが来なくてもデータは届いているケースが多いのでリトライはしない。
        if (response.indexOf("HTTP/1.1 302") >= 0)
        {
            client.stop();
            SerialDebug.println("######## start redirect");

            int locS = response.indexOf("https:");
            int locE = response.indexOf("\r\n", locS);
            int hostE = response.indexOf("/", locS + 8);

            //SerialDebug.println("\r\n########");
            //String AllRe = response.substring(locS, locE);
            //SerialDebug.println("All: " + AllRe);

            String reHost = response.substring(locS + 8, hostE);
            SerialDebug.println("redirect Host: " + reHost);

            String picUrl = response.substring(hostE, locE);
            SerialDebug.println("redirect url: " + picUrl);
            //SerialDebug.println("########\r\n");

            if (client.connect(reHost.c_str(), _PORT) > 0)
            {
                String getMsgRe = String("GET ") + picUrl + " HTTP/1.1\r\n" +
                                  "Host: " + reHost.c_str() + "\r\n" +
                                  "Accept: */*\r\n\r\n";

                size_t ret = client.print(getMsgRe);
                if (ret == 0)
                {
                    client.stop();
                    SerialDebug.println("########## Re;sendReport Send Error !! ###########\r\n");

                    return retVal;
                }

                //wait ack
                unsigned long timeout = millis();
                while (client.available() == 0)
                {
                    if (millis() - timeout > 10000)
                    {
                        SerialDebug.println(">>> Client Timeout !");
                        client.stop();
                        return false;
                    }
                    delay(100);
                }

                String response = client.readString();
                SerialDebug.println(response);
                retVal = true;

                SerialDebug.println("######## redirect done");
            }
            else
            {
                SerialDebug.println("######## redirect fail....");
            }
        }
        else if (response.indexOf("HTTP/1.1 500") >= 0)
        {
            SerialDebug.println("#### HTTP/1.1 500 Internal Server Error");
        }

        client.stop();
    }else{
        SerialDebug.println("#######  connection error");
    }

    SerialDebug.println("#######    sendReport end   #########\r\n");

    return retVal;
}

static xTaskHandle gss_task_handle;
static void gss_task(void *)
{
    int sendcount=0;

    portTickType xLastExecutionTime = xTaskGetTickCount();
    while (1)
    {
        int retryCnt = 0;

        modemL.wakeup();//wakeup the LTE-M modem

        while (retryCnt < 3)
        {
            if (sendReport(sendcount, retryCnt))
            {
                Dm.LedCtrl(RED_LED, OFF);
                break;
            }

            retryCnt++;

            delay(1000);
        }

        if (retryCnt >= 3)
        {
            doBeep(1);
            sendcount=0;
            
            // HTTP client errors
            SerialDebug.println("#### no connection or no HTTP server.");

            SerialDebug.println("### Start modem init again");

            if (!modem_init())
            {
                Dm.LedCtrl(RED_LED, ON);
                SerialDebug.println("### try again");
                if (!modem_init())
                {
                    Dm.reset();
                }
            }

            if (!setSSL())
            {
                Dm.LedCtrl(RED_LED, ON);
                SerialDebug.println("### try again");
                if (!setSSL())
                {
                    Dm.reset();
                }
            }
        }else{
            sendcount++;
        }

        modemL.sleep(); //sleep the LTE-M modem

        vTaskDelayUntil(&xLastExecutionTime, send_period_ms / portTICK_PERIOD_MS);
    }

    vTaskDelete(NULL);
}

void run_gss_task()
{
    SerialDebug.println("*** Hello world! ***");
    UartModem.begin(115200);

    xTaskCreate(gss_task, "gss_task", 2048, NULL, 3, &gss_task_handle);
}