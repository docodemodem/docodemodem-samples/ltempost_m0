/*
 * Sample program for DocodeModem　mini and LTE-M modem
 * Copyright (c) 2021 Circuit Desgin,Inc
 * Released under the MIT license
 */

#include <docodemo.h>
#include <HL7800ModemClient.h>
#include "modem_setting.h"
#include "gss_task.h"
#include "ssl_keys.h"

#define USE_SSL

DOCODEMO Dm = DOCODEMO();
HL7800Modem modemL;
HL7800ModemClient client(&modemL);

#ifdef USE_SSL
bool setSSL()
{
  SerialDebug.print("###initialize SSL...");
  bool ret;

  ret = client.setCACert(rootCA);
  if (!ret)
  {
    SerialDebug.println("err root_ca");
    return false;
  }

  ret = client.setCertificate(certificate);
  if (!ret)
  {
    SerialDebug.println("err certificate");
    return false;
  }

  ret = client.setPrivateKey(privateKey);
  if (!ret)
  {
    SerialDebug.println("err private_key");
    return false;
  }

  SerialDebug.println("OK");

  return true;
}
#endif

int writeUart(const uint8_t *data, uint32_t len)
{
  return UartModem.write(data, len);
}

int readUart(uint8_t *data, uint32_t len, uint32_t timeout)
{
  if (UartModem.available() > 0)
  {
    UartModem.setTimeout(timeout);
    return UartModem.readBytes(data, len);
  }
  else
  {
    vTaskDelay(1);
    return 0;
  }
}

bool modem_init()
{
  SerialDebug.print("###initialize LTE-M...");

  digitalWrite(RF_RESET_OUT, LOW);

  Dm.ModemPowerCtrl(OFF);
  vTaskDelay(10);
  Dm.ModemPowerCtrl(ON);

  digitalWrite(RF_RESET_OUT, HIGH);
  digitalWrite(RF_WAKEUP_OUT, HIGH);

  if (!modemL.init(&writeUart, &readUart))
  {
    SerialDebug.println("###init Error");
    return false;
  }

  SerialDebug.println("OK");

  SerialDebug.print("###Start activate...");

  if (!modemL.activate(APN, USERNAME, PASSWORD))
  {
    SerialDebug.println("###Error");
    return false;
  }
  SerialDebug.println("OK");

  char ip[16];
  if (modemL.getIPaddress(ip))
  {
    SerialDebug.print("#### ip: ");
    SerialDebug.println(ip);
  }

  std::string localtime;
  modemL.getlocaltime(&localtime);
  SerialDebug.print("LocalTime:");
  SerialDebug.println(localtime.c_str());

  std::string version;
  modemL.version(1,&version);
  SerialDebug.print("Version:");
  SerialDebug.println(version.c_str());

  return true;
}

TaskHandle_t Handle_main_task;
static void main_task(void *pvParameters)
{
  delay(2000); //wait for usb debug print

  SerialDebug.println("###Main task start");
  Dm.begin();

  Dm.LedCtrl(GREEN_LED, ON);

  UartModem.begin(115200);

  if (!modem_init())
  {
    Dm.LedCtrl(RED_LED, ON);
    SerialDebug.println("### try again");
    if (!modem_init())
    {
      Dm.reset();
    }
  }

#ifdef USE_SSL
  if (!setSSL())
  {
    Dm.LedCtrl(RED_LED, ON);
    SerialDebug.println("### try again");
    if (!setSSL())
    {
      Dm.reset();
    }
  }
#endif

  modemL.sleep(); //sleep the LTE-M modem

  Dm.LedCtrl(RED_LED, OFF);

  run_gss_task();

  vTaskDelete(NULL);
}

void setup()
{
  SerialDebug.begin(115200);

  vSetErrorSerial(&SerialDebug);

  xTaskCreate(main_task, "main_task", 512, NULL, tskIDLE_PRIORITY + 1, &Handle_main_task);

  vTaskStartScheduler();

  // error scheduler failed to start
  // should never get here
  while (1)
  {
    SerialDebug.println("Scheduler Failed! \n");
    delay(1000);
  }
}

void loop()
{
}
